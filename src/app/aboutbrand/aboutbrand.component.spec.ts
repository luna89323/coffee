import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutbrandComponent } from './aboutbrand.component';

describe('AboutbrandComponent', () => {
  let component: AboutbrandComponent;
  let fixture: ComponentFixture<AboutbrandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AboutbrandComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AboutbrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
