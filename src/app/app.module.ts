import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RankComponent } from './rank/rank.component';
import { InformationComponent } from './information/information.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { SeasonalComponent } from './seasonal/seasonal.component';
import { NewsComponent } from './news/news.component';
import { StoreComponent } from './store/store.component';
import { ContactComponent } from './contact/contact.component';
import { BrunchComponent } from './brunch/brunch.component';
import { AfternoonTeaComponent } from './afternoon-tea/afternoon-tea.component';
import { FooterComponent } from './footer/footer.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AboutbrandComponent } from './aboutbrand/aboutbrand.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RankComponent,
    InformationComponent,
    MenuComponent,
    HomeComponent,
    SeasonalComponent,
    NewsComponent,
    StoreComponent,
    ContactComponent,
    BrunchComponent,
    AfternoonTeaComponent,
    FooterComponent,
    AboutusComponent,
    AboutbrandComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
