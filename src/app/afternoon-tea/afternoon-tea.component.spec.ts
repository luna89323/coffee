import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AfternoonTeaComponent } from './afternoon-tea.component';

describe('AfternoonTeaComponent', () => {
  let component: AfternoonTeaComponent;
  let fixture: ComponentFixture<AfternoonTeaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AfternoonTeaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AfternoonTeaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
