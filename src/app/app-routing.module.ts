import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { NewsComponent } from './news/news.component';
import { SeasonalComponent } from './seasonal/seasonal.component';
import { StoreComponent } from './store/store.component';
import { ContactComponent } from './contact/contact.component';
import { BrunchComponent } from './brunch/brunch.component';
import { AfternoonTeaComponent } from './afternoon-tea/afternoon-tea.component';


const routes: Routes = [
  {path:"",redirectTo:"home",pathMatch:"full"},
  { path: "home", component: HomeComponent },
  { path: "menu", component: MenuComponent },
  {path:"seasonal",component:SeasonalComponent},
  {path:"news",component:NewsComponent},
  {path:"store",component:StoreComponent},
  {path:"contact",component:ContactComponent},
  {path:"brunch",component:BrunchComponent},
  {path:"afternoonTea",component:AfternoonTeaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
